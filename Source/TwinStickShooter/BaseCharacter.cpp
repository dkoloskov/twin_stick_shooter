// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

// ==================================================
void ABaseCharacter::calculateHealth(float delta)
{
	health += delta;
	calculateDead();
}

void ABaseCharacter::calculateDead()
{
	isDead = (health <= 0);
}

#if WITH_EDITOR
//Step 3: Implement the remainder of our helper code, used by the editor when we change values.
void ABaseCharacter::PostEditChangeProperty(FPropertyChangedEvent &propertyChangedEvent)
{
	isDead = false;
	health = 100;

	Super::PostEditChangeProperty(propertyChangedEvent);
	calculateDead();
}
#endif
