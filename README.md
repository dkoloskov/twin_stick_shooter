Project is based on the course: Twin Stick Shooter with Blueprints  
https://www.unrealengine.com/en-US/onlinelearning-courses/twin-stick-shooter-with-blueprints  
  
// ======================== CONTROLS ========================  
= Keyboard =  
	Movement: WASD  
	Rotate camera: Arrows, Mouse  
	Fire: LMB, Space  
	Quit: Esc  
= Gamepad =  
	Movement: Left Thumbstick  
	Rotate camera: Right Thumbstick  
	Fire: Right Trigger  
	Quit: ? (unasigned)  
  
// ======================== Known issues (Things that can be improved) ========================  
Note: issues below is left after finishing the course. It can be fixed, but, I don't want to invest time for this right now.  
  
	GAMEPLAY (most critical stuff)  
	- Fix rotation of a character with mouse. Current rotation functionality is done specifically for gamepad and has wrong behavior with mouse.  
	- Fix explosion position, move it to the center of the enemy body.  